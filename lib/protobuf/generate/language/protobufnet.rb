require 'protobuf/generate/language'

module Protobuf
  module Generate
    class Language
      module Helpers
        def map_type type
            mappings = Hash[*%w{int32 int int64 long sint32 int sint64 long uint32 uint uint64 ulong}]
            mappings[type] ? mappings[type] : type
        end

        def identifier_name field_name
            field_name.split('_').map(&:capitalize).join
        end

        def field_declarations
            self.fields.reject { |f| f.kind_of?(Protobuf::Generate::Ast::Enum) or f.kind_of?(Protobuf::Generate::Ast::Message) }
        end
      end

      class ProtobufNet < Language
        match     %r{^ pbnet $}x
        templates Dir.glob(File.join(File.expand_path(File.dirname(__FILE__)), 'protobufnet', '[^_]*.erb'))
      end # C#

    end # Langage
  end # Generate
end # Protobuf
